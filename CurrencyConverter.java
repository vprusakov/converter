package com.vprusakov.currencyconverter;

import android.os.AsyncTask;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;


class CurrencyConverter extends AsyncTask<String, Void, String> {

    CurrencyConverter(final CurrencyRequest request, final AsyncResponse delegate) {
        this.request = request;
        this.delegate = delegate;
        this.execute();
    }

    @Override
    protected String doInBackground(String... params) {
        String result = "error";
        try {
            final String XMLRates = downloadXMLRates();
            final String srcRate = parseXMLRate(request.srcCurrency, XMLRates);
            final String tgtRate = parseXMLRate(request.tgtCurrency, XMLRates);
            result = convert(srcRate, tgtRate);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onPostExecute(final String s) {
        delegate.processFinish(s);
    }

    static class CurrencyRequest {
        CurrencyRequest(float srcInput, String srcCurrency, String tgtCurrency) {
            this.srcCurrency = srcCurrency;
            this.srcInput = srcInput;
            this.tgtCurrency = tgtCurrency;
        }

        private final String srcCurrency;
        private final Float srcInput;
        private final String tgtCurrency;
    }

    interface AsyncResponse {
        void processFinish(String output);
    }

    private String downloadXMLRates() throws IOException {
        final URL myUrl = new URL("http://www.cbr.ru/scripts/XML_daily_eng.asp");
        final HttpURLConnection connection = (HttpURLConnection) myUrl.openConnection();
        connection.setRequestMethod("GET");
        connection.connect();

        final InputStreamReader streamReader = new
                InputStreamReader(connection.getInputStream());
        final BufferedReader reader = new BufferedReader(streamReader);

        String inputLine;
        final StringBuilder stringBuilder = new StringBuilder();
        while ((inputLine = reader.readLine()) != null) {
            stringBuilder.append(inputLine);
        }

        reader.close();
        streamReader.close();

        return stringBuilder.toString();
    }

    private String parseXMLRate(final String currency, final String XMLRates) throws XmlPullParserException, IOException {

        if (currency.equals("RUB")) return "1";

        final XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        final XmlPullParser xpp = factory.newPullParser();
        xpp.setInput(new StringReader(XMLRates));

        int eventType = xpp.getEventType();
        boolean currCharCodeFound = false;
        String result = null;

        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG && xpp.getName().equals("CharCode") && xpp.nextText().equals(currency)) {
                currCharCodeFound = true;
            }
            if (currCharCodeFound && eventType == XmlPullParser.START_TAG && xpp.getName().equals("Value")) {
                result = xpp.nextText();
                break;
            }
            eventType = xpp.next();
        }

        return result;
    }

    private String convert(final String srcRate, final String tgtRate) {
        final Float fSrcRate = Float.parseFloat(srcRate.replace(",", "."));
        final Float fTgtRate = Float.parseFloat(tgtRate.replace(",", "."));
        final float result = request.srcInput * fSrcRate * 1/fTgtRate;
        return Float.toString(result);
    }

    private final AsyncResponse delegate;

    private final CurrencyRequest request;

}
