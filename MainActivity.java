package com.vprusakov.currencyconverter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONException;

import java.io.IOException;
import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        srcInput = findViewById(R.id.SrcInput);
        srcCurrency = findViewById(R.id.SrcCurrency);
        tgtInput = findViewById(R.id.TgtInput);
        tgtCurrency = findViewById(R.id.TgtCurrency);
        Button saveButton = findViewById(R.id.saveButton);

        addTextWatchers();
        addSpinnerWatchers();

        saveButton.setOnClickListener(onSaveClickListener);

        try {
            history = new History(this, "history.json");
            refreshHistory();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void addSpinnerWatchers() {
        class CurrencySpinnerListener implements AdapterView.OnItemSelectedListener {

            @Override
            public void onItemSelected(final AdapterView<?> adapterView, final View view, final int i, final long l) {
                if (isFirstTime) {
                    isFirstTime = false;
                }
                else {
                    convert(srcInput, srcCurrency, tgtInput, tgtCurrency);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}


            private boolean isFirstTime = true;

        }

        final String[] currencies = getResources().getStringArray(R.array.currency_array);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, currencies);

        srcCurrency.setAdapter(adapter);
        srcCurrency.setOnItemSelectedListener(new CurrencySpinnerListener());
        tgtCurrency.setAdapter(adapter);
        tgtCurrency.setOnItemSelectedListener(new CurrencySpinnerListener());

    }

    private void addTextWatchers() {
        class InputAmountWatcher implements TextWatcher {
            private InputAmountWatcher(EditText fromInput, Spinner fromCurrency, EditText toInput, Spinner toCurrency) {
                this.fromInput = fromInput;
                this.fromCurrency = fromCurrency;
                this.toInput = toInput;
                this.toCurrency = toCurrency;
            }

            @Override
            public void afterTextChanged(final Editable s) {
                if (MainActivity.this.getCurrentFocus() == fromInput) {
                    if (s.toString().equals("")) {
                        toInput.setText("");
                    } else {
                        convert(fromInput, fromCurrency, toInput, toCurrency);
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
            }

            private final EditText fromInput;
            private final Spinner fromCurrency;
            private final EditText toInput;
            private final Spinner toCurrency;
        }

        srcInput.addTextChangedListener(new InputAmountWatcher(srcInput, srcCurrency, tgtInput, tgtCurrency));
        tgtInput.addTextChangedListener(new InputAmountWatcher(tgtInput, tgtCurrency, srcInput, srcCurrency));
    }

    private void refreshHistory() {

        final LinkedList<History.HistoryEntry> historyEntries = history.getHistory();
        final ListView listView = findViewById(R.id.history);

        ArrayAdapter<History.HistoryEntry> adapter = new ArrayAdapter<History.HistoryEntry>(this, android.R.layout.simple_list_item_2, android.R.id.text1, historyEntries) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                final View view = super.getView(position, convertView, parent);
                final TextView text1 = view.findViewById(android.R.id.text1);
                final TextView text2 = view.findViewById(android.R.id.text2);
                final History.HistoryEntry historyEntry = historyEntries.get(position);
                text1.setText(historyEntry.srcInput.concat(" ").concat(historyEntry.srcCurrency));
                text2.setText(historyEntry.tgtInput.concat(" ").concat(historyEntry.tgtCurrency));
                return view;
            }
        };

        listView.setAdapter(adapter);
    }

    private void convert(final EditText fromInput, final Spinner fromCurrency, final EditText toInput, final Spinner toCurrency) {
        new CurrencyConverter(
                new CurrencyConverter.CurrencyRequest(
                        Float.parseFloat(fromInput.getText().toString()),
                        fromCurrency.getSelectedItem().toString(),
                        toCurrency.getSelectedItem().toString()
                ),
                new CurrencyConverter.AsyncResponse() {
                    @Override
                    public void processFinish(String output) {
                        toInput.setText(output);
                    }
                });
    }

    private View.OnClickListener onSaveClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {
                history.addHistoryEntry(history.new HistoryEntry(srcInput.getText().toString(), srcCurrency.getSelectedItem().toString(), tgtInput.getText().toString(), tgtCurrency.getSelectedItem().toString()));
                refreshHistory();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private History history;
    private EditText srcInput;
    private Spinner srcCurrency;
    private EditText tgtInput;
    private Spinner tgtCurrency;
}
