package com.vprusakov.currencyconverter;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;

class History {

    History(final Context context, final String fileName) throws IOException, JSONException {
        this.context = context;
        this.fileName = fileName;

        final File file = new File(context.getFilesDir(), fileName);
        historyEntries = (file.exists()) ? parseJSONHistoryFile() : new LinkedList<HistoryEntry>();
    }

    LinkedList<HistoryEntry> getHistory() {
        return historyEntries;
    }

    void addHistoryEntry(HistoryEntry historyEntry) throws IOException, JSONException {
        final int limit = 10;
        if (historyEntries.size() == limit) {
            historyEntries.removeLast();
        }
        historyEntries.push(historyEntry);
        saveHistory();
    }

    private LinkedList<HistoryEntry> parseJSONHistoryFile() throws IOException, JSONException {
        final String jsonText = readFile();
        final JSONArray JSONHistoryEntries = new JSONArray(jsonText);
        final int historyEntriesCount = JSONHistoryEntries.length();
        final ArrayList<HistoryEntry> historyEntries = new ArrayList<>();

        for (int i = 0; i < historyEntriesCount; i++) {
            final JSONObject JSONEntry = JSONHistoryEntries.getJSONObject(i);
            historyEntries.add(new HistoryEntry(JSONEntry.getString("srcCurrency"), JSONEntry.getString("srcInput"), JSONEntry.getString("tgtCurrency"), JSONEntry.getString("tgtInput")));
        }
        return new LinkedList<>(historyEntries);
    }

    private JSONArray getHistoryJSONArray() throws JSONException {
        final JSONArray historyJSONArray = new JSONArray();
        for (HistoryEntry historyEntry : historyEntries) {
            final JSONObject JSONHistoryEntry = new JSONObject();
            JSONHistoryEntry.put("srcCurrency", historyEntry.srcCurrency);
            JSONHistoryEntry.put("srcInput", historyEntry.srcInput);
            JSONHistoryEntry.put("tgtCurrency", historyEntry.tgtCurrency);
            JSONHistoryEntry.put("tgtInput", historyEntry.tgtInput);
            historyJSONArray.put(JSONHistoryEntry);
        }
        return historyJSONArray;
    }

    private void saveHistory() throws JSONException, IOException {
        writeFile(getHistoryJSONArray().toString());
    }

    private String readFile() throws IOException {
        final FileInputStream fileInputStream = context.openFileInput(fileName);
        final InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
        final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        final StringBuilder sb = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            sb.append(line);
        }
        return sb.toString();
    }

    private void writeFile(String content) throws IOException {
        final FileOutputStream fOut = context.openFileOutput(fileName, Context.MODE_PRIVATE);
        fOut.write(content.getBytes());
        fOut.close();
    }

    class HistoryEntry {
        HistoryEntry(String srcCurrency, String srcInput, String tgtCurrency, String tgtInput) {
            this.srcCurrency = srcCurrency;
            this.srcInput = srcInput;
            this.tgtCurrency = tgtCurrency;
            this.tgtInput = tgtInput;
        }

        final String srcCurrency;
        final String srcInput;
        final String tgtCurrency;
        final String tgtInput;
    }

    private final LinkedList<HistoryEntry> historyEntries;
    private final Context context;
    private final String fileName;
}
